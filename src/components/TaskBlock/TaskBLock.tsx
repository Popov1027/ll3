import React, {useEffect, useState} from 'react';
import {TaskResponseInterface} from '../../services/task-response.interface';
import {useParams} from 'react-router-dom';
import {getTaskById} from '../../services/taskService';
import moment from 'moment/moment';

const TaskBLock = () => {
	const [task, setTask] = useState<TaskResponseInterface>();
	const {id} = useParams();
    
	useEffect(() => {
		getTaskById(id).then((data) => setTask(data));
	},[]);

	console.log('ssssssssssssssssss',task);

	return (
		<div className="flex flex-col justify-center items-center w-full">
			<div className="w-1/2 mt-20 border rounded-lg shadow-xl p-4 flex flex-col justify-center items-center">
				<div className="flex flex-col justify-center items-center">
					<p className="text-blue-700 font-bold text-3xl first-letter:uppercase">{task?.title}</p>
					<span className="text-gray-400 text-sm">Created at: {moment(task?.createdAt).format('Do MMM YYYY')}</span>
				</div>
				<div className="mt-12 text-xl text-gray-700 font-semibold">
					<p>{task?.description}</p>
				</div>
				<div className="flex justify-end items-end w-full mt-8 text-red-700 font-bold">
					<p>Date limit: {moment(task?.dueDate).format('Do MMM YYYY')}</p>
				</div>
			</div>
		</div>
	);
};

export default TaskBLock;