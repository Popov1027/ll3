import React from 'react';

const Footer = () => {
	return (
		<div className="absolute bottom-0 w-full">
			<footer className="bg-gray-300 shadow">
				<div className="w-full mx-auto max-w-screen-xl p-4 flex justify-center items-center">
					<span className="text-sm text-gray-500 sm:text-center">© 2023 Popov Serghei. All Rights Reserved.</span>
				</div>
			</footer>

		</div>
	);
};

export default Footer;