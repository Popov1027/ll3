import React from 'react';
import {TaskResponseInterface} from '../../services/task-response.interface';
import moment from 'moment';

const Favorites = () => {
	const favorites: TaskResponseInterface[] = JSON.parse(localStorage.getItem('favorites') || '[]');

	return (
		<div className="flex flex-col justify-center items-center w-full mt-28">
			<h1 className="text-4xl underline my-10 text-blue-950">Favorites tasks</h1>
			<div className="w-1/2 space-y-4">
				{favorites.length !== 0 ? (
					favorites.map((task) => (
						<div key={task.id} className="flex justify-between w-full border-b">
							<div className="text-blue-950 text-2xl">
								<h1>{task.title}</h1>
							</div>
							<div className="flex flex-col items-end space-y-1">
								<div className="text-gray-500 text-sm">
									<span>{moment(task.dueDate).format('Do MMM YYYY')}</span>
								</div>
							</div>
						</div>
					))
				) : (
					<div className="flex justify-center text-gray-500 text-3xl">There are no tasks</div>
				)}
			</div>
		</div>
	);
};

export default Favorites;