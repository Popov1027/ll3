import React, {FC} from 'react';
import {CreateTaskFormProps} from '../../services/task-response.interface';

const Form: FC<CreateTaskFormProps> = ({
	title,
	description,
	setDescription,
	setTitle,
	handleCreateTask,
	dueDate,
	setDueDate
}) => {
	return (
		<div className="flex justify-center">
			<div className="flex flex-col mt-40 px-12 shadow-xl rounded-xl w-[980px] p-3">
				<div className="flex justify-center text-blue-600 text-3xl font-bold">
					<h1>Create task</h1>
				</div>
				<form onSubmit={handleCreateTask} className="flex flex-col mt-3 w-full">
					<div className="mb-6">
						<label className="block mb-2 text-sm font-medium text-gray-900">Title*</label>
						<input
							type="text"
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							className="border text-blue-950 text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
						/>
					</div>
					<div className="mb-6">
						<label className="block mb-2 text-sm font-medium text-gray-900">Description*</label>
						<textarea
							value={description}
							onChange={(e) => setDescription(e.target.value)}
							className="border text-blue-950 text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
						/>
					</div>
					<div className="mb-6">
						<label className="block mb-2 text-sm font-medium text-gray-900">Date limit*</label>
						<input
							type="date"
							value={dueDate}
							onChange={(e) => setDueDate(e.target.value)}
							className="border text-blue-950 text-sm rounded-lg block w-full p-2.5 focus-visible:outline-0"
						/>
					</div>
					<div className="flex justify-end my-4">
						<button
							type='submit'
							className="w-40 rounded-full px-4 py-2 bg-blue-500 text-white border hover:bg-amber-500 transition duration-500 font-bold">
                            Submit
						</button>
					</div>
				</form>
			</div>
		</div>
	);
};

export default Form;