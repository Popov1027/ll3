import axios from 'axios';

const http = axios.create({
	baseURL: 'https://6547e016902874dff3acd516.mockapi.io/tasks/api/v1/'
});

export default {
	get: http.get,
	post: http.post,
	put: http.put,
	delete: http.delete,
	patch: http.patch
};
